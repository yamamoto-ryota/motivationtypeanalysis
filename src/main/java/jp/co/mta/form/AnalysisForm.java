package jp.co.mta.form;

public class AnalysisForm {
	private int departmentId;
	private int selectedTypeOption;

	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getSelectedTypeOption() {
		return selectedTypeOption;
	}
	public void setSelectedTypeOption(int selectedTypeOption) {
		this.selectedTypeOption = selectedTypeOption;
	}

}
