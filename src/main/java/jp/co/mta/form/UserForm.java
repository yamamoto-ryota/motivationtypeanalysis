package jp.co.mta.form;

import java.util.Date;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserForm {

	private int id;

	@Pattern(regexp = "^[0-9]*$", message = "半角数字7桁の社員番号を入力してください")
	@Size(min = 7, max = 7, message = "半角数字7桁の社員番号を入力してください")
	private String employeeNumber;

	@Size(min = 6, max = 20, message = "パスワードは6文字以上、20文字以下で設定してください")
	@Pattern(regexp = "^[a-zA-Z0-9]*$", message = "パスワードは半角英数字で入力してください")
	private String password;

	@Size(min = 6, max = 20, message = "確認用パスワードは6文字以上、20文字以下で設定してください")
	private String testPassword;

	@Size(max = 30, min= 1, message = "名前は30文字以下で入力してください")
	private String name;

	private int departmentId;
	private int isFinished;
	private int isStopped;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTestPassword() {
		return testPassword;
	}
	public void setTestPassword(String testPassword) {
		this.testPassword = testPassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(int isFinished) {
		this.isFinished = isFinished;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
