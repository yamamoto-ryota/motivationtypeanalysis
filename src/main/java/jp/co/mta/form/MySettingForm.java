package jp.co.mta.form;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class MySettingForm {
	private int id; //userId
	private int departmentId;

	@Size(max=7, min=7, message="社員番号は半角数字7桁で入力してください")
	private String employeeNumber;

	@Size(max=30, min=1, message="氏名は30字以内で入力してください")
	private String name;

	@Size(max=20, min=6, message="パスワードは6～20文字で入力してください")
	private String password;

	@NotBlank(message="確認用パスワードを入力してください")
	private String testPassword;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTestPassword() {
		return testPassword;
	}
	public void setTestPassword(String testPassword) {
		this.testPassword = testPassword;
	}

}
