package jp.co.mta.form;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class LoginForm {

	@Size(max = 7, min = 7, message = "社員番号を入力してください")
	private String employeeNumber;

	@NotEmpty(message = "パスワードを入力してください")
	private String password;

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employee_number) {
		this.employeeNumber = employee_number;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}