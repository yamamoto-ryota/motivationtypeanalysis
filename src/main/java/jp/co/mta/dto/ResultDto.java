package jp.co.mta.dto;

import java.util.Date;

public class ResultDto {

	private int id;
	private int userId;
	private String employeeNumber;
	private String name;
	private int departmentId;
	private String department;
	private int drivePoint;
	private int analysisPoint;
	private int createPoint;
	private int volunteerPoint;
	private Date createdDate;
	private String formattedCreatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public int getDrivePoint() {
		return drivePoint;
	}
	public void setDrivePoint(int drivePoint) {
		this.drivePoint = drivePoint;
	}
	public int getAnalysisPoint() {
		return analysisPoint;
	}
	public void setAnalysisPoint(int analysisPoint) {
		this.analysisPoint = analysisPoint;
	}
	public int getCreatePoint() {
		return createPoint;
	}
	public void setCreatePoint(int createPoint) {
		this.createPoint = createPoint;
	}
	public int getVolunteerPoint() {
		return volunteerPoint;
	}
	public void setVolunteerPoint(int volunteerPoint) {
		this.volunteerPoint = volunteerPoint;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getFormattedCreatedDate() {
		return formattedCreatedDate;
	}
	public void setFormattedCreatedDate(String formattedCreatedDate) {
		this.formattedCreatedDate = formattedCreatedDate;
	}


}
