package jp.co.mta.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import jp.co.mta.dto.ResultDto;
import jp.co.mta.dto.UserDto;
import jp.co.mta.form.AnalysisForm;
import jp.co.mta.form.ExamForm;
import jp.co.mta.form.LoginForm;
import jp.co.mta.form.MySettingForm;
import jp.co.mta.form.UserForm;
import jp.co.mta.service.MtaService;

@Controller
@SessionAttributes(value = "loginUser")
public class MtaController {

	@Autowired
	private MtaService mtaService;

	UserDto loginUser = new UserDto();
	int validNum;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String testInsert(Model model) {

		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		} else if (loginUser.getDepartmentId() != 1) {
			validNum = 1;
			return "redirect:./";
		}
		model.addAttribute("userForm", new UserForm());
		model.addAttribute("departments", mtaService.selectDepartments());
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String testInsert(@Valid @ModelAttribute("userForm") UserForm userForm, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("userForm", userForm);
			model.addAttribute("departments", mtaService.selectDepartments());
			return "signup";
		}else if(mtaService.selectUserByEmployeeNumber(userForm.getEmployeeNumber()) != null) {
			model.addAttribute("departments", mtaService.selectDepartments());
			model.addAttribute("message", "その社員番号は既に使用されています");
			model.addAttribute("userForm", userForm);
			return "signup";
		}else if(!userForm.getPassword().equals(userForm.getTestPassword())) {
			model.addAttribute("departments", mtaService.selectDepartments());
			model.addAttribute("message", "入力したパスワードと確認用パスワードが一致しません");
			model.addAttribute("userForm", userForm);
			return "signup";
		}
		mtaService.signUpMta(userForm);
		return "redirect:/management";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, Model model) {
		if(validNum == 2) {
			model.addAttribute("message", "ログインしてください");
			validNum = 0;
		}
		model.addAttribute("userForm", new LoginForm());
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String getFormInfo(@Valid @ModelAttribute("userForm") LoginForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("userForm", form);
			return "./login";
		}
		UserDto user = mtaService.loginMta(form);
		if (user.getId() == 0 || user.getIsStopped() == 1) {
			model.addAttribute("message", "社員番号またはパスワードが間違っています");
			model.addAttribute("userForm", form);
			return "login";
		}
		BeanUtils.copyProperties(user, loginUser);
		model.addAttribute("loginUser", loginUser);
		return "redirect:./";
	}

	@ModelAttribute("loginUser")
	public UserDto setloginuser() {
		return new UserDto();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {

		if (loginUser.getId() == 0) {
			validNum = 2;
		return "redirect:/login";
		}else {
			loginUser = mtaService.selectUserById(loginUser.getId());
		}
		if(validNum == 1) {
			model.addAttribute("message", "アクセス権限がありません");
			validNum = 0;
		}else if(validNum == 3) {
			model.addAttribute("message", "不正なパラメータが入力されました");
			validNum = 0;
		}else if(loginUser.getIsFinished() == 0) {
			model.addAttribute("message", "初めての方は、診断ボタンから診断を受けて下さい。");
		}
		model.addAttribute("showResultHome", mtaService.selectResultById(loginUser.getId()));
		return "index";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String loginOut(HttpSession session, SessionStatus sessionStatus) {
		session.removeAttribute("loginUser");
		session.invalidate();
		sessionStatus.setComplete();

		loginUser = new UserDto();

		return "redirect:/login";
	}

	@RequestMapping(value = "/exam", method = RequestMethod.GET)
	public String exam(Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return  "redirect:/login";
		}
		model.addAttribute("examForm", new ExamForm());
		return "exam";
	}

	@RequestMapping(value = "/exam", method = RequestMethod.POST)
	public String answer(@Valid @ModelAttribute("examForm") ExamForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("examForm", form);
			return "exam";
		}
		mtaService.insertResult(form);
		return "redirect:.myResult";
	}

	@RequestMapping(value = "/myResult", method = RequestMethod.GET)
	public String selectResult(Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		}
		model.addAttribute("userResults", mtaService.selectResultById(loginUser.getId()));
		return "result";
	}

	@RequestMapping(value = "/result", method = RequestMethod.GET)
	public String selectResultById(Model model, @ModelAttribute("id") int id) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		}
		model.addAttribute("userResults", mtaService.selectResultById(id));
		return "result";
	}

	@RequestMapping(value = "/mySetting", method = RequestMethod.GET)
	public String editMyself(Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		}
		model.addAttribute("userForm", new MySettingForm());
		return "mySetting";

	}

	@RequestMapping(value = "/mySetting", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String editMyself(@Valid @ModelAttribute("userForm") MySettingForm setting, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			return "mySetting";
		} else if (!setting.getPassword().equals(setting.getTestPassword())) {
			model.addAttribute("message", "入力したパスワードと確認用パスワードが一致しません");
			model.addAttribute("userForm", setting);
			return "mySetting";
		}
		UserForm form = new UserForm();
		BeanUtils.copyProperties(setting, form);
		mtaService.updateUser(form);
		return "redirect:./";
	}

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public String manageUser(HttpSession session, Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		} else if (loginUser.getDepartmentId() != 1) {
			validNum = 1;
			return "redirect:./";
		}
		model.addAttribute("loginUserId",loginUser.getId());
		model.addAttribute("userForm", new UserForm());
		model.addAttribute("allUsers", mtaService.selectAllUser());
		model.addAttribute("departments", mtaService.selectDepartments());

		return "management";
	}

	@RequestMapping(value = "/management/deleteUser", method = RequestMethod.POST)
	public String delete(@ModelAttribute("userForm") UserForm userForm, BindingResult result, Model model) {
		mtaService.deleteUser(userForm.getId());
		return "redirect:./";
	}

	@RequestMapping(value = "/management/switch", method = RequestMethod.POST)
	public String switchStatus(@ModelAttribute("userForm") UserForm userForm, BindingResult result, Model model) {
		mtaService.switchStatus(userForm);
		return "redirect:./";
	}

	@RequestMapping(value = "/setting", method = RequestMethod.GET)
	public String userEditShow(@ModelAttribute UserForm userForm, BindingResult result, Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		} else if (loginUser.getDepartmentId() != 1) {
			validNum = 1;
			return "redirect:./";
		}

		try {
		model.addAttribute("userForm", new UserForm());
		model.addAttribute("user", mtaService.selectUserById(userForm.getId()));
		model.addAttribute("departments", mtaService.selectDepartments());
		return "setting";
		}catch(IllegalArgumentException e) {
			validNum = 3;
			return "redirect:./";
		}
	}

	@RequestMapping(value = "/setting", method = RequestMethod.POST, produces = "text/plain;charset=utf-8")
	public String userEdit(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("departments", mtaService.selectDepartments());
			model.addAttribute("user", form);
			return "setting";
		}else if(mtaService.selectUserByEmployeeNumber(form.getEmployeeNumber()) != null) {
			if(mtaService.selectUserByEmployeeNumber(form.getEmployeeNumber()).getId() != form.getId()){
			model.addAttribute("message", "その社員番号は既に使用されています");
			model.addAttribute("user", form);
			model.addAttribute("departments", mtaService.selectDepartments());
			return "setting";
			}
		}else if(!form.getPassword().equals(form.getTestPassword())) {
			model.addAttribute("message", "入力したパスワードと確認用パスワードが一致しません");
			model.addAttribute("user", form);
			model.addAttribute("departments", mtaService.selectDepartments());
			return "setting";
		}
		mtaService.updateUser(form);
		return "redirect:/management";
	}

	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public String showStatistscs(Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		}

		List<ResultDto> result = mtaService.statistics();

		model.addAttribute("departments", mtaService.selectDepartments());
		model.addAttribute("userResult", result);
		model.addAttribute("analysisForm", new AnalysisForm());
		return "statistics";
	}

	@RequestMapping(value = "/comparison", method = RequestMethod.GET)
	public String getComparing(Model model) {
		validNum = 2;
		return "redirect:/login";
	}

	@RequestMapping(value = "/comparison", method = RequestMethod.POST)
	public String getComparing(@Valid @ModelAttribute AnalysisForm analysisForm, BindingResult result, Model model) {
		if (loginUser.getId() == 0) {
			validNum = 2;
			return "redirect:/login";
		}
		model.addAttribute("departments", mtaService.selectDepartments());
		model.addAttribute("comparedData", mtaService.selectResultByDepartment(analysisForm.getDepartmentId()));
		model.addAttribute("analysisForm",new AnalysisForm());
		return "comparison";
	}

	@RequestMapping(value = "/addDepartment", method = RequestMethod.GET)
	public String addDepartment(Model model) {
		return "addDepartment";
	}

	@RequestMapping(value = "/addDepartment", method = RequestMethod.POST)
	public String addDepartment(@ModelAttribute("department") String department, Model model) {
		mtaService.insertDepartment(department);
		return "redirect:/management";
	}
}
