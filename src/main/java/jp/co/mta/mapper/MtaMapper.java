package jp.co.mta.mapper;

import java.util.List;

import jp.co.mta.entity.Department;
import jp.co.mta.entity.Result;
import jp.co.mta.entity.User;

public interface MtaMapper {

	List<Department> selectDepartment();

	User loginMta(User user);

	void signUpMta(User user);

	void insertResult(Result result);

	List<Result> selectResult();

	List<Result> selectResultById(int id);

	List<Result> selectResultByDepartment(int departmentId);

	List<Result> selectResultByName(String name);

	User selectUserByEmployeeNumber(String employeeNumber);

	List<User> selectAllUser();

	User selectUserById(int id);

	void updateUser(User user);

	void deleteUser(int id);

	void deleteUsersResult(int id);

	void switchStatus(User user);

	void finishUser(User user);

	void insertDepartment(String department);

}