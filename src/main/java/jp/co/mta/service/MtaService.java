package jp.co.mta.service;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.mta.dto.DepartmentDto;
import jp.co.mta.dto.ResultDto;
import jp.co.mta.dto.UserDto;
import jp.co.mta.entity.Department;
import jp.co.mta.entity.Result;
import jp.co.mta.entity.User;
import jp.co.mta.form.ExamForm;
import jp.co.mta.form.LoginForm;
import jp.co.mta.form.UserForm;
import jp.co.mta.mapper.MtaMapper;
import jp.co.mta.utils.CipherUtil;

@Service
public class MtaService {

	@Autowired
	private MtaMapper mtaMapper;

	public void signUpMta(UserForm userForm) {
		User user = new User();
		String encPassword = CipherUtil.encrypt(userForm.getPassword());
		userForm.setPassword(encPassword);
		BeanUtils.copyProperties(userForm, user);
		mtaMapper.signUpMta(user);
		return;
	}

	public List<DepartmentDto> selectDepartments() {
		List<Department> departments = mtaMapper.selectDepartment();
		List<DepartmentDto> dtoDepartments = convertToDto(departments);
		return dtoDepartments;

	}

	private List<DepartmentDto> convertToDto(List<Department> departments) {
		List<DepartmentDto> resultList = new LinkedList<>();
		for (Department entity : departments) {
			DepartmentDto dto = new DepartmentDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	private List<ResultDto> resultConvertToDto(List<Result> resultEntity) {
		List<ResultDto> resultDto = new LinkedList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		for (Result entity : resultEntity) {
			ResultDto dto = new ResultDto();
			BeanUtils.copyProperties(entity, dto);
			if(entity.getCreatedDate() != null) {
			dto.setFormattedCreatedDate(dateFormat.format(entity.getCreatedDate()));
			}
			resultDto.add(dto);
		}
		return resultDto;
	}

	private List<UserDto> userConvertToDto(List<User> userEntity) {
		List<UserDto> userDto = new LinkedList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		for (User user : userEntity) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(user, dto);
			dto.setFormattedCreatedDate(dateFormat.format(user.getCreatedDate()));
			userDto.add(dto);
		}
		return userDto;
	}

	public UserDto loginMta(LoginForm userForm) {
		User user = new User();
		String encPassword = CipherUtil.encrypt(userForm.getPassword());
		userForm.setPassword(encPassword);
		BeanUtils.copyProperties(userForm, user);
		User resultUser = mtaMapper.loginMta(user);
		UserDto loginUser = new UserDto();
		if(resultUser == null) {
			return loginUser;
		}
		BeanUtils.copyProperties(resultUser, loginUser);
		return loginUser;
	}

	public void insertResult(ExamForm exam) {
		Result result = new Result();

		User user = mtaMapper.selectUserById(exam.getUserId());

		result.setUserId(user.getId());
		result.setName(user.getName());
		result.setDepartmentId(user.getDepartmentId());

		if (exam.getTest01_1() < 3) {
			if (exam.getTest01_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest01_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest01_2() < 3) {
			if (exam.getTest01_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest01_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest02_1() < 3) {
			if (exam.getTest02_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest02_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest02_2() < 3) {
			if (exam.getTest02_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest02_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest03_1() < 3) {
			if (exam.getTest03_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest03_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest03_2() < 3) {
			if (exam.getTest03_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest03_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest04_1() < 3) {
			if (exam.getTest04_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest04_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest04_2() < 3) {
			if (exam.getTest04_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest04_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest05_1() < 3) {
			if (exam.getTest05_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest05_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest05_2() < 3) {
			if (exam.getTest05_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest05_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest06_1() < 3) {
			if (exam.getTest06_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest06_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest06_2() < 3) {
			if (exam.getTest06_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest06_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest07_1() < 3) {
			if (exam.getTest07_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest07_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest07_2() < 3) {
			if (exam.getTest07_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest07_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest08_1() < 3) {
			if (exam.getTest08_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest08_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest08_2() < 3) {
			if (exam.getTest08_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest08_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest09_1() < 3) {
			if (exam.getTest09_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest09_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest09_2() < 3) {
			if (exam.getTest09_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest09_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		if (exam.getTest10_1() < 3) {
			if (exam.getTest10_1() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 2);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 2);
			}
		} else if (exam.getTest10_1() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 2);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 2);
		}

		if (exam.getTest10_2() < 3) {
			if (exam.getTest10_2() == 1) {
				result.setDrivePoint(result.getDrivePoint() + 1);
			} else {
				result.setAnalysisPoint(result.getAnalysisPoint() + 1);
			}
		} else if (exam.getTest10_2() == 3) {
			result.setCreatePoint(result.getCreatePoint() + 1);
		} else {
			result.setVolunteerPoint(result.getVolunteerPoint() + 1);
		}

		mtaMapper.insertResult(result);

		User userEntity = new User();
		if (user.getIsFinished() == 0) {
			user.setIsFinished(1);
		}

		BeanUtils.copyProperties(user, userEntity);
		mtaMapper.finishUser(userEntity);
	}

	public List<ResultDto> selectResult() {
		List<Result> resultEntity = mtaMapper.selectResult();
		List<ResultDto> resultDto = resultConvertToDto(resultEntity);
		return resultDto;
	}

	public List<ResultDto> selectResultById(int id) {
		List<Result> selectResult = mtaMapper.selectResultById(id);
		List<ResultDto> selectResultDto = resultConvertToDto(selectResult);
		return selectResultDto;
	}

	public List<ResultDto> selectResultByDepartment(int departmentId) {
		List<Result> selectResult = mtaMapper.selectResultByDepartment(departmentId);
		List<ResultDto> selectResultDto = resultConvertToDto(selectResult);
		return selectResultDto;

	}

	public UserDto selectUserById(int id) {
		User selectedUser = mtaMapper.selectUserById(id);
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(selectedUser, userDto);
		return userDto;
	}

	public User selectUserByEmployeeNumber(String EmployeeNumber) {
		User selectedUser = mtaMapper.selectUserByEmployeeNumber(EmployeeNumber);
		return selectedUser;
	}

	public List<UserDto> selectAllUser() {
		List<User> allUserEntity = mtaMapper.selectAllUser();
		List<UserDto> allUserDto = userConvertToDto(allUserEntity);
		return allUserDto;
	}

	public List<ResultDto> statistics() {
		List<Result> departmentResultEntity = new LinkedList<>();

		List<Result> resultsAll = mtaMapper.selectResult();
		if (!resultsAll.isEmpty()) {

			Result result = new Result();

			for (Result res : resultsAll) {
				result.setDrivePoint(result.getDrivePoint() + res.getDrivePoint());
				result.setAnalysisPoint(result.getAnalysisPoint() + res.getAnalysisPoint());
				result.setCreatePoint(result.getCreatePoint() + res.getCreatePoint());
				result.setVolunteerPoint(result.getVolunteerPoint() + res.getVolunteerPoint());
			}
			result.setDrivePoint(result.getDrivePoint() / resultsAll.size());
			result.setAnalysisPoint(result.getAnalysisPoint() / resultsAll.size());
			result.setCreatePoint(result.getCreatePoint() / resultsAll.size());
			result.setVolunteerPoint(result.getVolunteerPoint() / resultsAll.size());
			result.setDepartmentId(0);
			result.setDepartment("ALH");
			departmentResultEntity.add(result);

			for (int num = 1; num <= mtaMapper.selectDepartment().size(); num++) {
				List<Result> results = mtaMapper.selectResultByDepartment(num);
				Result sta = new Result();
				if (!results.isEmpty()) {
					for (Result res : results) {
						sta.setDrivePoint(sta.getDrivePoint() + res.getDrivePoint());
						sta.setAnalysisPoint(sta.getAnalysisPoint() + res.getAnalysisPoint());
						sta.setCreatePoint(sta.getCreatePoint() + res.getCreatePoint());
						sta.setVolunteerPoint(sta.getVolunteerPoint() + res.getVolunteerPoint());
					}
					sta.setDrivePoint(sta.getDrivePoint() / results.size());
					sta.setAnalysisPoint(sta.getAnalysisPoint() / results.size());
					sta.setCreatePoint(sta.getCreatePoint() / results.size());
					sta.setVolunteerPoint(sta.getVolunteerPoint() / results.size());
					sta.setDepartmentId(results.get(0).getDepartmentId());
					sta.setDepartment(results.get(0).getDepartment());
				}
				departmentResultEntity.add(sta);
			}
		}
		List<ResultDto> departmentResultDto = resultConvertToDto(departmentResultEntity);
		return departmentResultDto;
	}

	public void updateUser(UserForm form) {
		User user = new User();
		String encPassword = CipherUtil.encrypt(form.getPassword());
		form.setPassword(encPassword);
		BeanUtils.copyProperties(form, user);
		mtaMapper.updateUser(user);
	}

	public void switchStatus(UserForm form) {
		User user = new User();
		BeanUtils.copyProperties(form, user);
		if (user.getIsStopped() == 1) {
			user.setIsStopped(1);
		} else if (user.getIsStopped() == 0) {
			user.setIsStopped(0);
		}
		mtaMapper.switchStatus(user);
	}

	public void deleteUser(int id) {
		mtaMapper.deleteUser(id);
		mtaMapper.deleteUsersResult(id);
	}

	public void insertDepartment(String department) {
		mtaMapper.insertDepartment(department);
	}
}