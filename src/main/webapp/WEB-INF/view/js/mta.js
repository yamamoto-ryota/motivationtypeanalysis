window.addEventListener('DOMContentLoaded', function() {

	$("#gotoComparison").prop('disabled', true);

	jQuery(function() {
		 const selected = $("select[name=departmentId]");

		 selected.on('change', function(){
		        selectedVal = selected.val();
		        console.log(selectedVal);

		        if(selectedVal != "notSelected"){
					 $('#gotoComparison').prop('disabled',false);
				 }
		    });


		});





	/* comparison 画面のポップアップ　レーダーチャート */
	$('.chart').on('click', function() {
		var str = $(this).val();
		var arrValue = str.split(',');
		console.log(arrValue);

		var ctx = document.getElementById('myChart').getContext('2d');


		inputList = arrValue;
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis  = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);
		var titleName = inputList[4];

		var myChart = new Chart(ctx, {
		  type: 'radar',
		  data: {
		    labels: ['Drive', 'Analysis', 'Volunteer', 'Creative'],
		    datasets: [{

		      label: 'label',
		      data: [drive,analysis,volunteer, creative],
		      backgroundColor: "rgba(255,0,0,0.4)"
		    }]
		  },
			options : {

				title : { // タイトル
					display : true,
					fontSize : 20,
					text : titleName
				},
				scale : {
		               pointLabels: {       // 軸のラベル（"国語"など）
		                    fontSize: 16,         // 文字の大きさ
		                    fontColor: "rgba(0,0,0,0.7)"    // 文字の色
		            },
					ticks : { // 目盛り
						min : 0, // 最小値
						max : 20, // 最大値
						stepSize : 2, // 目盛の間隔
						fontSize : 12, // 目盛り数字の大きさ
						fontColor : "#3c4458" // 目盛り数字の色
					},
					angleLines : { // 軸（放射軸）
						display : true,
						color : 'rgba(0, 0, 0, 0.1)'
					},
					gridLines : { // 補助線（目盛の線）
						display : true,
						color : 'rgba(0, 0, 0, 0.1)'
					}
				},
				legend: {
					display: false
				}
			}
		});
	});


	jQuery(function() {
		jQuery('button', '.jquery-ui-button').button();
		jQuery('.jquery-ui-dialog-opener').click(function() {
			jQuery('#showChart').dialog('open');
		});

		jQuery('#showChart').dialog({
			autoOpen : false,
			width : 540,
			modal : true,
		});
	});







	$(document).ready(function() {
		$("#managementList").tablesorter({
			headers : {
				0 : {
					sorter : false
				},
				5 : {
					sorter : false
				},
				6 : {
					sorter : false
				},
				7 : {
					sorter : false
				},
				8 : {
					sorter : false
				}
			}
		});
	});

	$(document).ready(function() {
		$("#comparisonList").tablesorter({
			headers :{
				6:{
					sorter:false
				}
			}
		});
	});

});






function checkDisp(obj, id) {
	if (obj.checked) {
		document.getElementById(id).style.display = "block";
	} else {
		document.getElementById(id).style.display = "none";
	}
}

function deleteConfirm() {
	myRet = window.confirm("本当に削除します。よろしいですか？");
	if (myRet == true) {
		alert("削除しました。");
		return true;
	} else {
		alert("キャンセルされました。");
		return false;
	}
}
