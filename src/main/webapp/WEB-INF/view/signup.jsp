<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <link href="./css/mta.css" rel="stylesheet" type="text/css">
		<title>ユーザー新規登録</title>
	</head>
	<body>
	<div class="menu">
	   <ul>
	      <li><a href="${pageContext.request.contextPath}/">ホーム</a>
	      <li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
	      <li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	      <li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
	      <c:if test="${loginUser.departmentId == 1}">
		     <li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	      </c:if>
	      <li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
	   </ul>
	</div>



<div class="wrapper" >

		<h1>ユーザー新規登録</h1>

 		<c:if test="${not empty message }">
			<div class="error-message" >
				<c:out value="${message }" />
			</div>
		</c:if>



<div class="settingForm">
<form:form modelAttribute="userForm">
      <form:errors path="*" element="div" cssClass="errorMessages" />


		<dl>
              <dt>
                <form:label path="employeeNumber" class="setting">社員番号</form:label>
              </dt>
              <dd>
                 <form:input path="employeeNumber" class="setting" />
              </dd>
		      <dt>
                <form:label path="name" class="setting">氏名</form:label>
              </dt>
              <dd>
                 <form:input path="name" class="setting" />
              </dd>
              <dt>
                <form:label path="password" class="setting">パスワード</form:label>
              </dt>
              <dd>
                 <form:password path="password" class="setting" />
              </dd>
              <dt>
                <form:label path="testPassword" class="setting">確認用パスワード</form:label>
              </dt>
              <dd>
                 <form:password path="testPassword" class="setting" />
              </dd>
               <dt>
                <form:label path="departmentId" class="setting" >部署</form:label>
              </dt>
              <dd>


  			  <form:select path="departmentId" class="setting" >
                    <c:forEach items="${departments}" var="department">
                       <c:if test="${userForm.departmentId == department.id}">
           			      <option value="${department.id}" selected>
           			         <c:out value="${department.name}" />
        		          </option>
        		       </c:if>
        		       <c:if test="${userForm.departmentId != department.id}">
           			      <option value="${department.id}">
           			         <c:out value="${department.name}" />
        		          </option>
        		       </c:if>
      				</c:forEach>
       		   	</form:select>
              </dd>
             </dl>

             <div class="setting_button">
                <form:button >登録</form:button>
		     </div>

    </form:form>
</div>
</div>
	</body>
</html>
