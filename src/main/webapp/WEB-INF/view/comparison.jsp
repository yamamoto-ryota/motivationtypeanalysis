<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <meta charset="utf-8">
    <title>ユーザー比較</title>
    <style type="text/css"></style>

    <link href="./css/mta.css" rel="stylesheet" type="text/css">
    <script src="<c:url value="/js/mta.js" />"></script>

    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>

    <script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.0/css/theme.default.min.css">
    <!-- jQuery UI、TableSorter（外部プラグイン）をダウンロード -->
    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/start/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript"
	src="http://	ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
    <link rel=”stylesheet” type=”text/css” href=”${pageContext.request.contextPath}/css/todo.css” />

<!--  jQuery chart.js をダウンロード -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>

  </head>
  <body>
    <div class="antibug_menu">
		<ul>
			<li><a href="${pageContext.request.contextPath}/">ホーム</a>
			<li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
			<li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
			<li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
				<c:if test="${loginUser.departmentId == 1}">
					<li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
				</c:if>
			<li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
		</ul>
	</div>


<div class="wrapper">
    <h1>比較</h1>

    <div class="comparison_form">
    <form:form modelAttribute="analysisForm" action="${pageContext.request.contextPath}/comparison" >

	              <form:select path="departmentId" >
                       <c:forEach items="${departments}" var="department">
                       <c:if test="${department.id == comparedData[0].departmentId}" >
                       		<option value="${department.id}"  selected>
                       			<c:out value="${department.name }"/>
                       		</option>
                       </c:if>
           			   <c:if test="${department.id != comparedData[0].departmentId}" >
           			   <option value="${department.id}" >
           			     <c:out value="${department.name}" />
        		      </option>
        		      </c:if>
      				  </c:forEach>
       			</form:select>
       			<form:button >検索</form:button>

<!--
	    <form:select path="selectedTypeOption">
	    	<option disabled selected >タイプを選択してください</option>
	    	<option value="1">ドライブ</option>
	    	<option value="2">アナライズ</option>
	    	<option value="3">クリエイト</option>
	    	<option value="4">ボランティア</option>
	    	<option value="5">ドライブ＋アナライズ</option>
	    	<option value="6">ドライブ＋クリエイト</option>
	    	<option value="7">ボランティア＋アナライズ</option>
	    	<option value="8">ボランティア＋クリエイト</option>
	    </form:select>
-->
    </form:form>
    </div>

    <div class="usersInfo">


   <table border="1" class="tablesorter"  id="comparisonList" >
        <thead>
          <tr>
          <th class="name1">名前</th>
          <th class="employeeNumber1">社員番号</th>
          <th class="point">ドライブ</th>
          <th class="point">アナライズ</th>
          <th class="point">ボランティア</th>
          <th class="point" >クリエイト</th>

          <th class="buttons">チャート閲覧</th>
          </tr>
        </thead>

     <tbody>
      <c:forEach items="${comparedData}" var="user">
			<tr class="userinfo">
	            <td class="id">
	              <span class="info">
	                <c:out value="${user.name}">
	                </c:out>
	              </span>
	            </td>
	            <td class="employeeNumber">
	              <span >
					 <c:out value="${user.employeeNumber}">
	                </c:out>
	              </span>
	            </td>
	            <td class="drive">
	              <span class="">
	                <c:out value="${user.drivePoint}">
	                </c:out>
	              </span>
	            </td>
	            <td class="analysis">
	              <span class=" ">
	                <c:out value="${user.analysisPoint}">
	                </c:out>
	              </span>
	            </td>
	            <td class="volunteer">
	              <span >
	                <c:out value="${user.volunteerPoint}">
	                </c:out>
	              </span>
	            </td>
	            <td class="create">
	              <span >
	                <c:out value="${user.createPoint}">
	                </c:out>
	              </span>
	            </td>
	            <td>
	              <!--  レーダーチャート閲覧 -->
	              <button class="chart jquery-ui-dialog-opener"  value="${user.drivePoint},${user.analysisPoint},${user.volunteerPoint},${user.createPoint},${user.name}">閲覧</button>

	            </td>
            </tr>
          </c:forEach>
        </tbody>
      </table>


  </div>
</div>

<div id="showChart" title="チャート">
  <canvas id="myChart" width=“300px” height=“300px”></canvas>
</div>


</body>
</html>