<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>統計</title>
<link href="./css/mta.css" rel="stylesheet" type="text/css">
<script src="<c:url value="/js/mta.js" />"></script>
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

</head>
<body>
	<div class="antibug_menu">
		<ul>
			<li><a href="${pageContext.request.contextPath}/">ホーム</a>
			<li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
			<li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
			<li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
				<c:if test="${loginUser.departmentId == 1}">
					<li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
				</c:if>
			<li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
		</ul>
	</div>


<div class="wrapper">
	<h1 class="statistics">部署統計</h1>

<!--
	<div>
	<span>aaa</span>
	<c:forEach items="${userResult}" var="departmentResult" varStatus="status" >
		<span>department ID :</span><c:out value="${departmentResult.departmentId }"></c:out><br/>
		<span>department name :</span><c:out value="${departmentResult.department }"></c:out><br/>
		<span>drive:</span><c:out value="${departmentResult.drivePoint }"></c:out><br/>
		<span>analysis:</span><c:out value="${departmentResult.analysisPoint }"></c:out><br/>
		<span>volunteer:</span><c:out value="${departmentResult.volunteerPoint }"></c:out><br/>
		<span>create:</span><c:out value="${departmentResult.createPoint }"></c:out><br/>

<span>ID:</span><c:out value="${analysisForm.departmentId }"></c:out><br/>

	<form:form modelAttribute="analysisForm"
	action="${pageContext.request.contextPath}/comparison"  method="post">
	<form:hidden path ="departmentId"  value= "${departmentResult.departmentId}"/>
		<input type="submit" id="examConfirm" value="送信">
	</form:form>
	</c:forEach>

	</div>
 -->
    <div class="comparison_form">
	   <form:form modelAttribute="analysisForm"
		   action="${pageContext.request.contextPath}/comparison" >
		   <form:select path="departmentId"  class="comparisonOption">
			   <option disabled selected value="notSelected">選択してください</option>
			   <c:forEach items="${departments}"  var="department" >
				   <option value="${department.id}"><c:out value="${department.name }" />
			   </c:forEach>
		   </form:select>

		<form:button  id="gotoComparison">検索</form:button>

	</form:form>
	</div>


	<div class="charts">
		<c:forEach items="${userResult}" var="departmentResult" varStatus="status">

            <div class="chart">
			   <input type="hidden"
				      value="${departmentResult.drivePoint},${departmentResult.analysisPoint},${departmentResult.volunteerPoint},${departmentResult.createPoint}"
				      class="examScore${status.count}" />
  			   <input type="hidden" value="${departmentResult.department}"  class="titleName${status.count}" />

			   <canvas id="myChart${status.count}" width=“500px” height=“500px”></canvas>
			</div>


		</c:forEach>
	</div>



</div>

	<script>
	for(let i = 1; i <= 24; i++){
		var ctx = document.getElementById('myChart' + i).getContext('2d');
		var inputArr = $('.examScore' + i).val();
		var titleName = $('.titleName' + i).val();

		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);
		console.log("titleName: " + titleName)

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
				labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
				datasets : [ {

					label : 'label',
					data : [ drive, analysis, volunteer, creative ],
					backgroundColor : "rgba(255,0,0,0.4)"

				} ]
			},

		options : {
					title : { // タイトル
						display : true,
						fontSize : 20,
						text : titleName
					},
					scale : {
			               pointLabels: {       // 軸のラベル（"国語"など）
			                    fontSize: 16,         // 文字の大きさ
			                    fontColor: "rgba(0,0,0,0.7)"    // 文字の色
			            },
						ticks : { // 目盛り
							min : 0, // 最小値
							max : 20, // 最大値
							stepSize : 2, // 目盛の間隔
							fontSize : 12, // 目盛り数字の大きさ
							fontColor : "#3c4458" // 目盛り数字の色
						},
						angleLines : { // 軸（放射軸）
							display : true,
							color : 'rgba(0, 0, 0, 0.1)'
						},
						gridLines : { // 補助線（目盛の線）
							display : true,
							color : 'rgba(0, 0, 0, 0.1)'
						}
					},
					legend : {
						display : false
					}
				}
			});
		}
	</script>

</body>
</html>