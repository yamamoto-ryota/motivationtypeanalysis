<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta charset="utf-8">
<title>診断画面</title>
<style type="text/css"></style>

<link href="<c:url value="/css/mta.css" />" rel="stylesheet">
<script src="<c:url value="/js/mta.js" />"></script>
<script src="<c:url value="/js/mtaExam.js" />"></script>

<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>

</head>
<body>

	<div class="antibug_menu">
		<ul>
			<li><a href="${pageContext.request.contextPath}/">ホーム</a>
			<li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
			<li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
			<li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>

				<c:if test="${loginUser.departmentId == 1}">
					<li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
				</c:if>
			<li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
		</ul>
	</div>

	<input type="hidden"  id="preInputExam"
		value="${examForm.test01_1},
					${examForm.test01_2},
					${examForm.test02_1},
					${examForm.test02_2},
					${examForm.test03_1},
					${examForm.test03_2},
					${examForm.test04_1},
					${examForm.test04_2},
					${examForm.test05_1},
					${examForm.test05_2},
					${examForm.test06_1},
					${examForm.test06_2},
					${examForm.test07_1},
					${examForm.test07_2},
					${examForm.test08_1},
					${examForm.test08_2},
					${examForm.test09_1},
					${examForm.test09_2},
					${examForm.test10_1},
					${examForm.test10_2},
					 " />

	<div class="wrapper">

		<h1 class="exam">タイプ診断</h1>

		<div class="examForm">
			<form:form modelAttribute="examForm" action="${pageContext.request.contextPath}/exam">
				<span class="error-message-list"> <c:out value="${errorMessage}" /></span>

				<div class="answer-wrapper">
					<span class="question">1.日常のクセは</span>


					<div class="answer1">
						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test01_1" element="div" cssClass="error-message" />

						<input id="test01_1_1" class="radio-inline_input_drive" type="radio" name="test01_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test01_1_1">場を仕切ろうとする</label><br>
						<input id="test01_1_2" class="radio-inline_input_analyse" type="radio" name="test01_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test01_1_2">何事もすぐ分析しようとする</label><br>
						<input id="test01_1_3" class="radio-inline_input_create" type="radio" name="test01_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test01_1_3">人と違ったことをしようとする</label><br>
						<input id="test01_1_4" class="radio-inline_input_volunteer" type="radio" name="test01_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test01_1_4">自己主張よりも場をまとめることを優先させようとする</label><br>

					</div>

					<div class="answer1">
						<div class="applicable">【次にあてはまる】</div>
						<form:errors path="test01_2" element="div" cssClass="error-message" />
						<input id="test01_2_1" class="radio-inline_input_drive" type="radio" name="test01_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test01_2_1">場を仕切ろうとする</label><br>
						<input id="test01_2_2" class="radio-inline_input_analyse" type="radio" name="test01_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test01_2_2">何事もすぐ分析しようとする</label><br>
						<input id="test01_2_3" class="radio-inline_input_create" type="radio" name="test01_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test01_2_3">人と違ったことをしようとする</label><br>
						<input id="test01_2_4" class="radio-inline_input_volunteer" type="radio" name="test01_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test01_2_4">自己主張よりも場をまとめることを優先させようとする</label><br>
					</div>

				</div>
				<div class="answer-wrapper">
					<span class="question">2.言われてカチンとくる言葉は</span>
					<div class="answer2">

					<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test02_1" element="div" cssClass="error-message" />

						<input id="test02_1_1" class="radio-inline_input_drive" type="radio" name="test02_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test02_1_1">「あの人には絶対ついていきたくないよね」</label><br>
						<input id="test02_1_2" class="radio-inline_input_analyse" type="radio" name="test02_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test02_1_2">「あの人、本当はなにもわかっていないよ」</label><br>
						<input id="test02_1_3" class="radio-inline_input_create" type="radio" name="test02_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test02_1_3">「あの人、何の変哲もない人よ」</label><br>
						<input id="test02_1_4" class="radio-inline_input_volunteer" type="radio" name="test02_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test02_1_4">「あの人、実は偽善者なんじゃない？」</label><br>

					</div>

					<div class="answer2">
					   <div class="applicable">【次にあてはまる】</div>
						<form:errors path="test02_2" element="div" cssClass="error-message" />
						<input id="test02_2_1" class="radio-inline_input_drive" type="radio" name="test02_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test02_2_1">「あの人には絶対ついていきたくないよね」</label><br>
						<input id="test02_2_2" class="radio-inline_input_analyse" type="radio" name="test02_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test02_2_2">「あの人、本当はなにもわかっていないよ」</label><br>
						<input id="test02_2_3" class="radio-inline_input_create" type="radio" name="test02_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test02_2_3">「あの人、何の変哲もない人よ」</label><br>
						<input id="test02_2_4" class="radio-inline_input_volunteer" type="radio" name="test02_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test02_2_4">「あの人、実は偽善者なんじゃない？」</label><br>

					</div>


				</div>


				<div class="answer-wrapper">
					<span class="question">3.恥ずかしいと感じる瞬間は</span>
					<div class="answer3">
						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test03_1" element="div" cssClass="error-message" />

						<input id="test03_1_1" class="radio-inline_input_drive" type="radio" name="test03_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test03_1_1">周囲に宣言したことが実現できなかったとき</label><br>
						<input id="test03_1_2" class="radio-inline_input_analyse" type="radio" name="test03_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test03_1_2">自分の専門領域の質問に対して、適切な返答が出来なかったとき</label><br>
						<input id="test03_1_3" class="radio-inline_input_create" type="radio" name="test03_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test03_1_3">自分のした仕事が二番煎じと言われたとき</label><br>
						<input id="test03_1_4" class="radio-inline_input_volunteer" type="radio" name="test03_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test03_1_4">私利私欲に目がくらんだ行動をとってしまったとき</label><br>

				     </div>

				     <div class="answer3">
						<div class="applicable">【次にあてはまる】</div>
						<form:errors path="test03_2" element="div" cssClass="error-message" />
						<input id="test03_2_1" class="radio-inline_input_drive" type="radio" name="test03_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test03_2_1">周囲に宣言したことが実現できなかったとき</label><br>
						<input id="test03_2_2" class="radio-inline_input_analyse" type="radio" name="test03_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test03_2_2">自分の専門領域の質問に対して、適切な返答が出来なかったとき</label><br>
						<input id="test03_2_3" class="radio-inline_input_create" type="radio" name="test03_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test03_2_3">自分のした仕事が二番煎じと言われたとき</label><br>
						<input id="test03_2_4" class="radio-inline_input_volunteer" type="radio" name="test03_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test03_2_4">私利私欲に目がくらんだ行動をとってしまったとき</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">4.共感度の高いフレーズは</span>
					<div class="answer4">
					<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test04_1" element="div" cssClass="error-message" />

						<input id="test04_1_1" class="radio-inline_input_drive" type="radio" name="test04_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test04_1_1">一国一城の主</label><br>
						<input id="test04_1_2" class="radio-inline_input_analyse" type="radio" name="test04_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test04_1_2">論より証拠</label><br>
						<input id="test04_1_3" class="radio-inline_input_create" type="radio" name="test04_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test04_1_3">芸は身を助く</label><br>
						<input id="test04_1_4" class="radio-inline_input_volunteer" type="radio" name="test04_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test04_1_4">縁の下の力持ち</label><br>

					</div>

					<div class="answer4">
						<div class="applicable">【次にあてはまる】</div>
						<form:errors path="test04_2" element="div" cssClass="error-message" />
						<input id="test04_2_1" class="radio-inline_input_drive" type="radio" name="test04_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test04_2_1">一国一城の主</label><br>
						<input id="test04_2_2" class="radio-inline_input_analyse" type="radio" name="test04_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test04_2_2">論より証拠</label><br>
						<input id="test04_2_3" class="radio-inline_input_create" type="radio" name="test04_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test04_2_3">芸は身を助く</label><br>
						<input id="test04_2_4" class="radio-inline_input_volunteer" type="radio" name="test04_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test04_2_4">縁の下の力持ち</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">5.最高だと思う瞬間は</span>
					<div class="answer5">
						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test05_1" element="div" cssClass="error-message" />

						<input id="test05_1_1" class="radio-inline_input_drive" type="radio" name="test05_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test05_1_1">困難な目標を達成できたとき</label><br>
						<input id="test05_1_2" class="radio-inline_input_analyse" type="radio" name="test05_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test05_1_2">「わかった！」と複雑なことの本質が見抜けたとき</label><br>
						<input id="test05_1_3" class="radio-inline_input_create" type="radio" name="test05_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test05_1_3">自分の個性を生かした新しいアイディアが周囲の注目を集めたとき</label><br>
						<input id="test05_1_4" class="radio-inline_input_volunteer" type="radio" name="test05_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test05_1_4">自分の行為が感謝されたとき</label><br>

					</div>

					<div class="answer5">
						<div class="applicable">【次にあてはまる】</div>
						<form:errors path="test05_2" element="div" cssClass="error-message" />
						<input id="test05_2_1" class="radio-inline_input_drive" type="radio" name="test05_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test05_2_1">困難な目標を達成できたとき</label><br>
						<input id="test05_2_2" class="radio-inline_input_analyse" type="radio" name="test05_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test05_2_2">「わかった！」と複雑なことの本質が見抜けたとき</label><br>
						<input id="test05_2_3" class="radio-inline_input_create" type="radio" name="test05_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test05_2_3">自分の個性を生かした新しいアイディアが周囲の注目を集めたとき</label><br>
						<input id="test05_2_4" class="radio-inline_input_volunteer" type="radio" name="test05_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test05_2_4">自分の行為が感謝されたとき</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">6.耐えられないことは</span>
					<div class="answer6">
						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test06_1" element="div" cssClass="error-message" />

						<input id="test06_1_1" class="radio-inline_input_drive" type="radio" name="test06_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test06_1_1">自分の同意できない話が目の前で進んでいるときに、発言しないでいること</label><br>
						<input id="test06_1_2" class="radio-inline_input_analyse" type="radio" name="test06_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test06_1_2">関心事に没頭し、取り組んでいる際に、軽率な発言によって邪魔されること</label><br>
						<input id="test06_1_3" class="radio-inline_input_create" type="radio" name="test06_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test06_1_3">自分の自信作が、「この人には評価してほしい」と思う人に受け入れてもらえないこと</label><br>
						<input id="test06_1_4" class="radio-inline_input_volunteer" type="radio" name="test06_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test06_1_4">皆の利害を調整していて、結果的にまとめられずに終わってしまうこと</label><br>

					</div>


					<div class="answer6">
						<div class="applicable">【次にあてはまる】</div>
						<form:errors path="test06_2" element="div" cssClass="error-message" />
						<input id="test06_2_1" class="radio-inline_input_drive" type="radio" name="test06_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test06_2_1">自分の同意できない話が目の前で進んでいるときに、発言しないでいること</label><br>
						<input id="test06_2_2" class="radio-inline_input_analyse" type="radio" name="test06_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test06_2_2">関心事に没頭し、取り組んでいる際に、軽率な発言によって邪魔されること</label><br>
						<input id="test06_2_3" class="radio-inline_input_create" type="radio" name="test06_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test06_2_3">自分の自信作が、「この人には評価してほしい」と思う人に受け入れてもらえないこと</label><br>
						<input id="test06_2_4" class="radio-inline_input_volunteer" type="radio" name="test06_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test06_2_4">皆の利害を調整していて、結果的にまとめられずに終わってしまうこと</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">7.自分の行動の傾向は</span>
									<div class="answer7">

						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test07_1" element="div" cssClass="error-message" />

						<input id="test07_1_1" class="radio-inline_input_drive" type="radio" name="test07_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test07_1_1">能力が自分と同等以上の人に 対して強い対抗心を燃やす</label><br>
						<input id="test07_1_2" class="radio-inline_input_analyse" type="radio" name="test07_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test07_1_2">物事の仕組みがわかると 説明せずにいられない</label><br>
						<input id="test07_1_3" class="radio-inline_input_create" type="radio" name="test07_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test07_1_3">面白いアイディアが浮かぶと、誰かに言わずにいられない</label><br>
						<input id="test07_1_4" class="radio-inline_input_volunteer" type="radio" name="test07_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test07_1_4">相手の好き嫌いにかかわらず、何かと世話をやいてしまう</label><br>

					</div>

					<div class="answer7">

						<div class="applicable">【次に当てはまる】</div>
						<form:errors path="test07_2" element="div" cssClass="error-message" />
						<input id="test07_2_1" class="radio-inline_input_drive" type="radio" name="test07_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test07_2_1">能力が自分と同等以上の人に 対して強い対抗心を燃やす</label><br>
						<input id="test07_2_2" class="radio-inline_input_analyse" type="radio" name="test07_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test07_2_2">物事の仕組みがわかると 説明せずにいられない</label><br>
						<input id="test07_2_3" class="radio-inline_input_create" type="radio" name="test07_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test07_2_3">面白いアイディアが浮かぶと、誰かに言わずにいられない</label><br>
						<input id="test07_2_4" class="radio-inline_input_volunteer" type="radio" name="test07_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test07_2_4">相手の好き嫌いにかかわらず、何かと世話をやいてしまう</label><br>
					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">8.自慢の象徴は</span>

					<div class="answer8">

						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test08_1" element="div" cssClass="error-message" />
						<input id="test08_1_1" class="radio-inline_input_drive" type="radio" name="test08_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test08_1_1">自分の影響力・人気</label><br>
						<input id="test08_1_2" class="radio-inline_input_analyse" type="radio" name="test08_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test08_1_2">自分の知識技能</label><br>
						<input id="test08_1_3" class="radio-inline_input_create" type="radio" name="test08_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test08_1_3">自分のアイディア・作品</label><br>
						<input id="test08_1_4" class="radio-inline_input_volunteer" type="radio" name="test08_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test08_1_4">自分への感謝の言葉</label><br>

					</div>

					<div class="answer8">

						<div class="applicable">【次に当てはまる】</div>
						<form:errors path="test08_2" element="div" cssClass="error-message" />
						<input id="test08_2_1" class="radio-inline_input_drive" type="radio" name="test08_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test08_2_1">自分の影響力・人気</label><br>
						<input id="test08_2_2" class="radio-inline_input_analyse" type="radio" name="test08_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test08_2_2">自分の知識技能</label><br>
						<input id="test08_2_3" class="radio-inline_input_create" type="radio" name="test08_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test08_2_3">自分のアイディア・作品</label><br>
						<input id="test08_2_4" class="radio-inline_input_volunteer" type="radio" name="test08_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test08_2_4">自分への感謝の言葉</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">9.目指す姿は</span>

					<div class="answer9">
						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test09_1" element="div" cssClass="error-message" />
						<input id="test09_1_1" class="radio-inline_input_drive" type="radio" name="test09_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test09_1_1">いつも高い目標に向かって走っていたい</label><br>
						<input id="test09_1_2" class="radio-inline_input_analyse" type="radio" name="test09_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test09_1_2">自分で導き出した答えにこだわりを持っていたい</label><br>
						<input id="test09_1_3" class="radio-inline_input_create" type="radio" name="test09_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test09_1_3">周りからアッと言われるようなことをしたい</label><br>
						<input id="test09_1_4" class="radio-inline_input_volunteer" type="radio" name="test09_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test09_1_4">人のためになることに一生懸命取り組みたい</label><br>
					</div>

					<div class="answer9">

					    <div class="applicable">【次にあてはまる】</div>
						<form:errors path="test09_2" element="div" cssClass="error-message" />
						<input id="test09_2_1" class="radio-inline_input_drive" type="radio" name="test09_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test09_2_1">いつも高い目標に向かって走っていたい</label><br>
						<input id="test09_2_2" class="radio-inline_input_analyse" type="radio" name="test09_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test09_2_2">自分で導き出した答えにこだわりを持っていたい</label><br>
						<input id="test09_2_3" class="radio-inline_input_create" type="radio" name="test09_2"value="3" />
						<label class="radio-inline_label_create" for="test09_2_3">周りからアッと言われるようなことをしたい</label><br>

						<input id="test09_2_4" class="radio-inline_input_volunteer" type="radio" name="test09_2"value="4" />
						<label class="radio-inline_label_volunteer" for="test09_2_4">人のためになることに一生懸命取り組みたい</label><br>

					</div>

				</div>


				<div class="answer-wrapper">
					<span class="question">10.周囲から指摘を受ける言葉は</span>

					<div class="answer10">

						<div class="applicable">【最もあてはまる】</div>
						<form:errors path="test10_1" element="div" cssClass="error-message" />
						<input id="test10_1_1" class="radio-inline_input_drive" type="radio" name="test10_1"
						 value="1" />
						<label class="radio-inline_label_drive" for="test10_1_1">人の意見を聞かないよね</label><br>
						<input id="test10_1_2" class="radio-inline_input_analyse" type="radio" name="test10_1"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test10_1_2">細かすぎるよね</label><br>
						<input id="test10_1_3" class="radio-inline_input_create" type="radio" name="test10_1"
						 value="3" />
						<label class="radio-inline_label_create" for="test10_1_3">きまぐれだよね</label><br>
						<input id="test10_1_4" class="radio-inline_input_volunteer" type="radio" name="test10_1"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test10_1_4">いい人になり過ぎるよね</label><br>

					</div>

					<div class="answer10">

						<div class="applicable">【次に当てはまる】</div>
						<form:errors path="test10_2" element="div" cssClass="error-message" />
						<input id="test10_2_1" class="radio-inline_input_drive" type="radio" name="test10_2"
						 value="1" />
						<label class="radio-inline_label_drive" for="test10_2_1">人の意見を聞かないよね</label><br>
						<input id="test10_2_2" class="radio-inline_input_analyse" type="radio" name="test10_2"
						 value="2" />
						<label class="radio-inline_label_analyse" for="test10_2_2">細かすぎるよね</label><br>
						<input id="test10_2_3" class="radio-inline_input_create" type="radio" name="test10_2"
						 value="3" />
						<label class="radio-inline_label_create" for="test10_2_3">きまぐれだよね</label><br>
						<input id="test10_2_4" class="radio-inline_input_volunteer" type="radio" name="test10_2"
						 value="4" />
						<label class="radio-inline_label_volunteer" for="test10_2_4">いい人になり過ぎるよね</label><br>

					</div>

			    </div>


				<form:hidden path="userId" value="${loginUser.id}" />
				<div class="send_box">
				   <input type="submit" id="examConfirm" value="送信!" class="send">
				</div>

			</form:form>

		</div>

	</div>

</body>
</html>