<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="./css/mta.css" rel="stylesheet" type="text/css">
    <title>部署追加</title>
	</head>
	<body>
	<div class="menu">
	   <ul>
	      <li><a href="${pageContext.request.contextPath}/">ホーム</a>
	      <li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
	      <li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	      <li><a href="${pageContext.request.contextPath}/result">過去の診断結果</a>
	      <c:if test="${loginUser.departmentId == 1}">
		     <li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	      </c:if>
	      <li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
	   </ul>
	</div>
	<div class="wrapper">
	  <h1>部署追加</h1>

	  	<c:if test="${not empty message }">
			<div class="error-message" >
				<c:out value="${message }" />
			</div>
		</c:if>

       <div class="settingForm">
        <form:form class="setting">
        <form:errors path="*" element="div" cssClass="errorMessages" />
		  <dl>
              <dt class="add">
                <label id="department" class="setting">部署名</label>
              </dt>
              <dd>
                 <input name="department" id="department" class="setting" />
              </dd>
             </dl>

              <div class="add_button">
                 <input type="submit" class="button" value="登録">
              </div>

           </form:form>
    </div>
   </div>
  </body>
</html>
