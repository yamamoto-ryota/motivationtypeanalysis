<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
   <meta charset="utf-8">
   <title>ホーム</title>
   <link href="./css/mta.css" rel="stylesheet" type="text/css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<body>
	<div class="antibug_menu">
	   <ul>
	      <li><a href="${pageContext.request.contextPath}/">ホーム</a>
	      <li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
	      <li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	      <li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
	      <c:if test="${loginUser.departmentId == 1}">
		     <li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	      </c:if>
	      <li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
	   </ul>
	</div>

	<div class="wrapper">
		<h1 class="home">
			<c:out value="${loginUser.name}" />のホーム
		</h1>

		<c:if test="${not empty message }">
			<div class="error-message" >
				<c:out value="${message }" />
			</div>
		</c:if>

		<div class="test_bottun_box">
			<h2><a href="${pageContext.request.contextPath}/exam" class="test_button">診断する！</a></h2>
        </div>
		<!-- Drive Analysis Volunteer Creative の順で渡す -->
		<input type="hidden" value="${showResultHome[0].drivePoint},${showResultHome[0].analysisPoint},${showResultHome[0].volunteerPoint},${showResultHome[0].createPoint}"
			class="examScore" />


		<div class="chart">
			<canvas id="myChart" width=“250px” height=“250px”></canvas>
		</div>
	</div>

	<script>

		var ctx = document.getElementById('myChart').getContext('2d');
		var inputArr = $('.examScore').val();
		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
		      labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
		      datasets : [ {

			  label : 'label',
			  data : [ drive, analysis, volunteer, creative ],
			  backgroundColor : "rgba(255,0,0,0.4)"

			  } ]
		    },
			options : {
						/*title : { // タイトル
							display : true,
							fontSize : 20,
							text : titleName
						},*/
						scale : {

		pointLabels : { // 軸のラベル（"国語"など）
						fontSize : 16, // 文字の大きさ
						fontColor : "rgba(0,0,0,0.7)" // 文字の色
					},
					ticks : { // 目盛り
						min : 0, // 最小値
						max : 20, // 最大値
						stepSize : 2, // 目盛の間隔
						fontSize : 12, // 目盛り数字の大きさ
						fontColor : "#3c4458" // 目盛り数字の色
					},
					angleLines : { // 軸（放射軸）
						display : true,
						color : 'rgba(0, 0, 0, 0.1)'
					},
					gridLines : { // 補助線（目盛の線）
						display : true,
						color : 'rgba(0, 0, 0, 0.1)'
					}

				},
				 legend: {
					display: false
				 }
			}
		});


	</script>



</body>
</html>