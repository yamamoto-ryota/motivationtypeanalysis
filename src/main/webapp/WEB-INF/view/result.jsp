<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <meta charset="utf-8">
    <title>診断結果</title>
    <style type="text/css"></style>

    <link href="./css/mta.css" rel="stylesheet" type="text/css">
    <link href="./png/typeResultAndDetails.png" type="png">
    <script src="<c:url value="/js/mta.js" />"></script>

    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

  </head>
  <body>

    <div class="antibug_menu">
       <ul>
          <li><a href="${pageContext.request.contextPath}/">ホーム</a>
          <li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
	      <li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	      <li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
	      <c:if test="${loginUser.departmentId == 1}">
		     <li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	      </c:if>
	      <li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>
	   </ul>
	</div>



	<!-- Drive Analysis Volunteer Creative の順で渡す -->

    <div class="result_main">

    <h1><c:out value="${userResults[0].name }の診断結果"  /></h1>

	<div class="charts">
	  <div class="chart">
	     <span class="exam_date"><c:out value="「${userResults[0].formattedCreatedDate}」の診断結果" /></span>
	     <input type="hidden"
	            value="${userResults[0].drivePoint},${userResults[0].analysisPoint},${userResults[0].volunteerPoint},${userResults[0].createPoint}" class="examScore1" />
	     <canvas id="myChart1" width=“300px” height=“300px”></canvas>
	  </div>

	  <div class="chart">
	     <span class="exam_date"><c:out value="「${userResults[1].formattedCreatedDate}」の診断結果" /></span>
         <input type="hidden"
                value="${userResults[1].drivePoint},${userResults[1].analysisPoint},${userResults[1].volunteerPoint},${userResults[1].createPoint}" class="examScore2" />
         <canvas id="myChart2" width=“300px” height=“300px”></canvas>
      </div>

      <div class="chart">
         <span class="exam_date"><c:out value="「${userResults[2].formattedCreatedDate}」の診断結果" /></span>
         <input type="hidden"
                value="${userResults[2].drivePoint},${userResults[2].analysisPoint},${userResults[2].volunteerPoint},${userResults[2].createPoint}" class="examScore3" />
         <canvas id="myChart3" width=“300px” height=“300px”></canvas>
      </div>
    </div>

    </div>


	 <div class="image_frame">
		<img src="./png/typeResultAndDetails.png" alt="タイプ別詳細" title="typeResultAndDetails.png" class="image">
	 </div>

	<script>

		var ctx = document.getElementById('myChart1').getContext('2d');
		var inputArr = $('.examScore1').val();
		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
		      labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
		      datasets : [ {

			  label : 'label',
			  data : [ drive, analysis, volunteer, creative ],
			  backgroundColor : "rgba(255,0,0,0.4)"

			  } ]
		    },
			options : {
				/*title : { // タイトル
					display : true,
					fontSize : 20,
					text : titleName
				},*/

				scale : {

pointLabels : { // 軸のラベル（"国語"など）
				fontSize : 16, // 文字の大きさ
				fontColor : "rgba(0,0,0,0.7)" // 文字の色
			},
			ticks : { // 目盛り
				min : 0, // 最小値
				max : 20, // 最大値
				stepSize : 2, // 目盛の間隔
				fontSize : 12, // 目盛り数字の大きさ
				fontColor : "#3c4458" // 目盛り数字の色
			},
			angleLines : { // 軸（放射軸）
				display : true,
				color : 'rgba(0, 0, 0, 0.1)'
			},
			gridLines : { // 補助線（目盛の線）
				display : true,
				color : 'rgba(0, 0, 0, 0.1)'
			}
				},
		         legend: {
		             display: false
		          },
			}
		});

	</script>

	<script>

		var ctx = document.getElementById('myChart2').getContext('2d');
		var inputArr = $('.examScore2').val();
		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
		      labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
		      datasets : [ {

			  label : 'label',
			  data : [ drive, analysis, volunteer, creative ],
			  backgroundColor : "rgba(255,0,0,0.4)"

			  } ]
		    },
		options : {
			/*title : { // タイトル
				display : true,
				fontSize : 20,
				text : titleName
			},*/
			scale : {

pointLabels : { // 軸のラベル（"国語"など）
			fontSize : 16, // 文字の大きさ
			fontColor : "rgba(0,0,0,0.7)" // 文字の色
		},
		ticks : { // 目盛り
			min : 0, // 最小値
			max : 20, // 最大値
			stepSize : 2, // 目盛の間隔
			fontSize : 12, // 目盛り数字の大きさ
			fontColor : "#3c4458" // 目盛り数字の色
		},
		angleLines : { // 軸（放射軸）
			display : true,
			color : 'rgba(0, 0, 0, 0.1)'
		},
		gridLines : { // 補助線（目盛の線）
			display : true,
			color : 'rgba(0, 0, 0, 0.1)'
		}
			},
        legend: {
            display: false
         },
		}
		});

	</script>

	<script>

		var ctx = document.getElementById('myChart3').getContext('2d');
		var inputArr = $('.examScore3').val();
		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
		      labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
		      datasets : [ {

			  label : 'label',
			  data : [ drive, analysis, volunteer, creative ],
			  backgroundColor : "rgba(255,0,0,0.4)"

			  } ]
		    },
		    options : {
				/*title : { // タイトル
					display : true,
					fontSize : 20,
					text : titleName
				},*/
				scale : {

pointLabels : { // 軸のラベル（"国語"など）
				fontSize : 16, // 文字の大きさ
				fontColor : "rgba(0,0,0,0.7)" // 文字の色
			},
			ticks : { // 目盛り
				min : 0, // 最小値
				max : 20, // 最大値
				stepSize : 2, // 目盛の間隔
				fontSize : 12, // 目盛り数字の大きさ
				fontColor : "#3c4458" // 目盛り数字の色
			},
			angleLines : { // 軸（放射軸）
				display : true,
				color : 'rgba(0, 0, 0, 0.1)'
			},
			gridLines : { // 補助線（目盛の線）
				display : true,
				color : 'rgba(0, 0, 0, 0.1)'
			}
				},
	         legend: {
	             display: false
	          },
			}
		});

	</script>



</body>
</html>