<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
   <meta charset="utf-8">
   <title>ログイン</title>
   <link href="./css/mta.css" rel="stylesheet" type="text/css">
</head>
<body>
   <div class="login_outer">
   <div class="login">
    <h1>ログイン</h1>

	<c:if test="${not empty message }">
		<div class="error-message" >
		  <c:out value="${message }" />
		</div>
	</c:if>

   <div class="loginForm">

   <form:form modelAttribute="userForm">
      <div class="error_messages">
         <form:errors path="*" element="div" cssClass="error-message-list" />
      </div>
      <dl>

         <dt>
            <form:label path="employeeNumber"  >社員番号</form:label>
         </dt>

         <dd>
            <span class="employeeNumber"><form:input path="employeeNumber" value="${employeeNumber}" class="setting" /></span>
         </dd>

         <dt>
            <span class="password"><form:label path="password"  >パスワード</form:label></span>
         </dt>

         <dd>
            <form:password path="password" value="${password}" class="setting" />
         </dd>

      </dl>
      <div class="login_button">
         <form:button >ログイン</form:button>
      </div>
   </form:form>

    </div>
   </div>
  </div>
</body>
</html>
