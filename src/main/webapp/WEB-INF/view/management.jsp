<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <meta charset="utf-8">
    <title>管理者ホーム画面</title>
    <style type="text/css"></style>

    <link href="<c:url value="/css/mta.css" />" rel="stylesheet">
    <script src="<c:url value="/js/mta.js" />"></script>

    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>

    <script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <!-- jQuery UI、TableSorter（外部プラグイン）をダウンロード -->
    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/start/jquery-ui.css" rel="stylesheet">
    <script type="text/javascript"
	src="http://	ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
    <link rel=”stylesheet” type=”text/css” href=”${pageContext.request.contextPath}/css/todo.css” />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.0/css/theme.default.min.css">
<!--  jQuery chart.js をダウンロード -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>

  </head>
  <body>

 <div class="antibug_menu">
       <ul>
          <li><a href="${pageContext.request.contextPath}/">ホーム</a>
          <li><a href="${pageContext.request.contextPath}/mySetting">パスワード変更</a>
	      <li><a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	      <li><a href="${pageContext.request.contextPath}/myResult">過去の診断結果</a>
	      <c:if test="${loginUser.departmentId == 1}">
		     <li><a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	      </c:if>
	      <li><a href="${pageContext.request.contextPath}/logout">ログアウト</a>

	   </ul>
	</div>

<div class="wrapper">
    <h1>管理者画面</h1>
    <div class="management_menu">
      <h2 class="signup"><a href="${pageContext.request.contextPath}/signup" class="signup">新規ユーザー登録</a></h2>
      <h2 class="add"><a href="${pageContext.request.contextPath}/addDepartment" class="signup">新規部署登録</a></h2>
    </div>

    <div class="usersInfo">

   <table border="1"  id="managementList"  class="tablesorter" >
        <thead>
          <tr>
          <th class="name">名前</th>
          <th class="employeeNumber">社員番号</th>
          <th class="isFinished">完了状態</th>
          <th class="department">部署</th>
          <th class="showResult">登録日</th>
          <th class="edit_button">編集</th>
          <th class="delete_button">削除</th>
          <th class="switch_button">切替</th>
          <th class="gotoResult">結果</th>
          </tr>
        </thead>

     <tbody>
      <c:forEach items="${allUsers}" var="user">
			<tr class="userinfo">
	            <td class="name">
	              <span class="info">
	                <c:out value="${user.name}">
	                </c:out>
	              </span>
	            </td>
	            <td class="employeeNumber">
	              <span >
					 <c:out value="${user.employeeNumber}">
	                </c:out>
	              </span>
	            </td>
	            <td class="isFinished">
	              <span class="">
	                <c:if test="${user.isFinished == 1}" >
	                	<span>完</span>
	                </c:if>
	                <c:if test="${user.isFinished != 1}" >
	                	<span>未完</span>
	                </c:if>
	              </span>
	            </td>
	            <td class="department">
	              <span class="">
	                <c:forEach items="${departments}" var="department">
	                  <c:if test="${user.departmentId == department.id}">
                         <c:out value="${department.name}" />
                      </c:if>
	                </c:forEach>
	              </span>
	            </td>
	            <td class="showResult">
	              <span class="">
	                <c:out value="${user.formattedCreatedDate}">
	                </c:out>
	              </span>
	            </td>

<!--  編集ボタン -->
                <td class="edit_button">
	       		   <form:form modelAttribute="userForm"
	       		    action="${pageContext.request.contextPath}/setting"  method="get" class="edit">
  					   <form:hidden path="id" value="${user.id}" />

               		   <form:button>編集</form:button>
				   </form:form>
				</td>


<!--  削除ボタン -->
                <td class="delete_button">
                   <c:if test="${loginUser.id != user.id}">
				     <form:form modelAttribute="userForm" onSubmit="return deleteConfirm()"
				     action="${pageContext.request.contextPath}/management/deleteUser" class="delete" >
  					     <form:hidden path="id" value="${user.id}" />
               		     <form:button>削除</form:button>
				     </form:form>
				   </c:if>
				</td>

<!--  停止/復活ボタン -->
                <td class="switch_button">
				   <c:if test="${loginUserId != user.id }">
					   <form:form modelAttribute="userForm"
			 		   action="${pageContext.request.contextPath}/management/switch" class="stop" >
			 	 	   <c:if test="${user.isStopped == 0}">
			 	   		   <form:hidden path="isStopped"  value="1" />
                 		   <form:hidden path="id"  value="${user.id }"/>
                		   <form:button class="stop">停止</form:button>
               		   </c:if>
                	   <c:if test="${user.isStopped != 0}">
                  		   <form:hidden path="isStopped" value="0" />
                  		   <form:hidden path="id"  value="${user.id }"/>
                 		   <form:button class="continue">復活</form:button>
                	   </c:if>
					   </form:form>
				   </c:if>
				</td>
				<td>
					<form:form modelAttribute="userForm"
						action="${pageContext.request.contextPath}/result"  method="get">
					<form:hidden path="id"  value="${user.id }" />
					<form:button>閲覧</form:button>
					</form:form>
				</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>


  </div>
</div>

<div id="showChart" title="チャート">
  <canvas id="myChart" width=“300px” height=“300px”></canvas>
</div>


</body>
</html>